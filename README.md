# Unnamed OS

This is the manifest repository.

## Preparing

You need to install `ninja`, `go`, `clang`, `make` and `llvm`.

To run the emulators, install `qemu` and, if necessary, the variant for your architecture.
Make sure you have VT-d/VT-x (or equivalents) enabled for emulation.

### Ubuntu

```
sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt update
sudo apt install golang-go ninja-build
```

Follow instructions on https://apt.llvm.org/ under section "Automatic installation script"
or "Ubuntu". Usually, all you need is this:

```
curl https://apt.llvm.org/llvm.sh | bash -
sudo apt install clang-14 libllvm14 llvm-14 llvm-14-runtime lld-14
```

### Arch Linux

```
sudo pacman -Syu go ninja clang llvm lld
```

## Getting the source code

### Installing repo

To sync sources, follow [this guide on setting up repo](https://source.android.com/setup/develop/repo).

In essence, this boils down to following commands:

```
mkdir -p ~/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
export PATH="$HOME/bin:$PATH"
echo -e '\nexport PATH="$HOME/bin:$PATH"' >> ~/.bashrc
```

(Instead of ~/.bashrc you may replace it by the shell rc file you use)

Note: the repo tool is not part of this software but used to synchronize the source code.

### Initializing source tree

Create a new directory you wish to use as project source tree, and `cd` into it.
Then, inside that directory run following command to initialize the source tree:

```
repo init -u https://gitlab.com/unnamed-os/manifest.git -b main
```

### Downloading source code

Downloading the source code the first time may take a while since it's going
to be downloading all bits and pieces needed to build this OS as well as the OS
components themselves. Be patient.

```
repo sync -j4 -c --no-clone-bundle --no-tags
```

### Synchronizing source code in the future

If you have the sources downloaded, you may use following command to sync all repositories:

```
./reposync
```

This is a shell script that contains the command above – nothing special.
It's just so that you don't need to remember the command.

## Building

Note: you need basic developer tools and libraries installed.
If you are a developer, these are more likely than not already installed.
Basic developer tools include `git`, `make`, `clang` (LLVM), and all the standard un*x utilities.

Building is currently only known to work on Linux (although you might have luck with BSDs and macOS).

The build tool used here is ninja. It's very fast and allows for a great incremental compilation experience.
Make is used as a wrapper, all it does is build the ninja build file generator, run it, and start ninja.

Building all components of the OS is fairly straightforward.
Run following command and everything should work out just fine:

```
TARGET_DEVICE=<device> make
```

Examples for device:
 - generic-x86_64

Supported devices are in the `device` directory.

Build results are saved to the `out` directory.

If you want to start the emulator run following command:

```
make start-<arch>
```

Examples for arch:
 - x86_64

If you want to have ninja do a build and then start the emulator, run:

```
TARGET_DEVICE=<device> make debug-<arch>
```

Most of the time incremental compilation should make it easy enough to test your changes straight away.

## License

Components of this OS are generally licensed under the GPLv3 license or later.
Find one of following files in the respective source directory for more information on the respective license:
 - LICENSE
 - COPYING
 - README.txt
 - README.md

You may also find that one of the first lines in source files include `SPDX-License-Identifier`.
This specifies the license according to https://spdx.dev/resources/use/.
